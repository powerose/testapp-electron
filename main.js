const createLoginWindow = require('./components/login/loginWindow');
const electron = require('electron');
const { shell, app, BrowserWindow } = electron;
const HOSTNAME = 'testapp.ga';
const HOMEPAGE = 'https://' + HOSTNAME + '/';

let mainWindow;

app.on('ready', () => {
    window = new BrowserWindow({
        width: 1200,
        height: 900,
        webPreferences: {
            nodeIntegration: false
        }
    });
    window.setMenuBarVisibility(false);
    window.loadURL(HOMEPAGE);

    window.webContents.on('will-navigate', (ev, url) => {
        if (!url.includes(HOSTNAME)) {
            ev.preventDefault();
            shell.openExternal(url);
        };
    });

    window.on('closed', () => {
        window = null;
    });
});

app.on('login', (event, webContents, request, authInfo, callback) => {
    // for http authentication
    event.preventDefault();

    createLoginWindow(callback);
});