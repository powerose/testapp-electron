# <img src="https://testapp.ga/assets/bear-standard.png" height="56" width="56" /> TestApp Electron

Learn better with TestApp!

![Current version](https://img.shields.io/badge/dynamic/json?label=Current%20version&query=version&url=https%3A%2F%2Fgitlab.com%2Ftestapp-system%2Ftestapp-electron%2Fraw%2Fmaster%2Fpackage.json&style=for-the-badge) ![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/testapp-system/testapp-electron?style=for-the-badge) [![testapp-desktop](https://snapcraft.io//testapp-desktop/badge.svg)](https://snapcraft.io/testapp-desktop)

The desktop wrapper for [TestApp](https://gitlab.com/testapp-system/). Supports Proxy authentication.

# Download

Download for:

 * [Windows](https://gitlab.com/testapp-system/testapp-electron/-/jobs/artifacts/master/browse/dist?job=win)
    - .exe amd64 & i386 (System and user installer)
    - .msi amd64 (System installer only)
    - .msi i386 (System installer only)
 * [macOS](https://gitlab.com/testapp-system/testapp-electron/-/jobs/artifacts/master/browse/dist?job=mac)
    - dmg
 * [Linux PC (32bit and 64bit)](https://gitlab.com/testapp-system/testapp-electron/-/jobs/artifacts/master/browse/dist?job=linux-pc)
    - Debian package (.deb)
    - RPM package
    - tar.xz
    - AppImage
 * [Linux ARM (armv7l and arm64)](https://gitlab.com/testapp-system/testapp-electron/-/jobs/artifacts/master/browse/dist?job=linux-arm)
    - Debian package (.deb)
    - RPM package
    - tar.xz
    - AppImage

## License

This software is licensed under the terms and conditions of `EUPL-1.2`.
