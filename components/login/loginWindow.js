const { BrowserWindow, ipcMain } = require("electron");
path = require('path');

Object.defineProperty(exports, "__esModule", {
  value: true
});

function createLoginWindow(loginCallback) {
  const loginWindow = new BrowserWindow({
    width: 300,
    height: 400,
    frame: false,
    resizable: false,
    webPreferences: {
      nodeIntegration: true,
    },
  });
  loginWindow.loadURL(
    `file://${path.join(__dirname.substr(0,__dirname.substr(0,__dirname.length-1).lastIndexOf("/")), '/../static/login/login.html')}`,
  );

  ipcMain.once('login-message', (event, usernameAndPassword) => {
    loginCallback(usernameAndPassword[0], usernameAndPassword[1]);
    loginWindow.close();
  });
  return loginWindow;
}

module.exports = createLoginWindow;
